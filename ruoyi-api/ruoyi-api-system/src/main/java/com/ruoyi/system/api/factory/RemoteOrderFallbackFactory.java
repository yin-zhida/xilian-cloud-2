package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.RemoteOrderService;
import com.ruoyi.system.api.domain.kernel.KernelOrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;

import java.util.List;

public class RemoteOrderFallbackFactory implements FallbackFactory<RemoteOrderService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteOrderFallbackFactory.class);



    @Override
    public RemoteOrderService create(Throwable throwable) {
        log.error("订单服务调用失败:{}", throwable.getMessage());
        return new RemoteOrderService()
        {


            @Override
            public AjaxResult editOrder(KernelOrderDTO kernelOrderDTO, String source) throws Exception {
                return null;
            }

            @Override
            public List<KernelOrderDTO> select(KernelOrderDTO kernelOrderDTO, String source) throws Exception {
                return null;
            }
        };
    }
}
