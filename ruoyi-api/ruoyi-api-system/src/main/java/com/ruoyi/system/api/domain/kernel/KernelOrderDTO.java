package com.ruoyi.system.api.domain.kernel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单对象 kernel_order
 *
 * @author lixin
 * @date 2023-08-29
 */
public class KernelOrderDTO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Integer id;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNumber;

    /** 订单状态（0支付成功，1待支付，2待退款） */
    @Excel(name = "订单状态", readConverterExp = "0=支付成功，1待支付，2待退款")
    private String orderState;

    /** 面试状态 */
    @Excel(name = "确认下单时间")
    private String interviewStatus;

    /** 描述 */
    @Excel(name = "面试会议号")
    private String version;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Excel(name = "面试开始时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm:ss")
    private Date placeOrderDate;

    /** 预定时间（面试者与面试官） */
    @Excel(name = "面试结束时间", readConverterExp = "面=试者与面试官")
    private Date reservationDate;

    /** 取消时间（同上） */
    @Excel(name = "取消时间", readConverterExp = "同=上")
    private Date cancelDate;

    /** 回复时间（最终面试官确认时间） */
    @Excel(name = "回复时间", readConverterExp = "最=终面试官确认时间")
    private Date replyDate;

    /** 产品id（对应产品id） */
    @Excel(name = "产品id", readConverterExp = "对=应产品id")
    private Integer productId;

    /** 求职者id(对应sys_user中userid) */
    @Excel(name = "求职者id(对应sys_user中userid)")
    private Integer userJobSeekerId;

    /** 面试官id(对应sys_user中userid) */
    @Excel(name = "面试官id(对应sys_user中userid)")
    private Integer userIntervieweeId;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payMode;

    /** 优惠方式（0原价，1优惠卷，2满减活动等） */
    @Excel(name = "优惠方式", readConverterExp = "0=原价，1优惠卷，2满减活动等")
    private String preferentialMode;

    /** 实付金额 */
    @Excel(name = "实付金额")
    private BigDecimal actualAmount;

    /** 原价 */
    @Excel(name = "原价")
    private BigDecimal originalCost;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String orderName;

    /** 订单详情 */
    @Excel(name = "订单详情")
    private String orderParticulars;

    /** 订单时长 */
    @Excel(name = "订单时长")
    private String orderDuration;

    /** 订单类型（0服务卡，1会员充值） */
    @Excel(name = "订单类型", readConverterExp = "0=服务卡，1会员充值")
    private String orderType;

    /** 会员类型（0月，1年，2自动续费月，3自动续费年） */
    @Excel(name = "会员类型", readConverterExp = "0=月，1年，2自动续费月，3自动续费年")
    private String orderMType;
    @Excel(name = "下单时间(创建订单时间)", width = 30, dateFormat = "yyyy-MM-dd hh:mm:ss")
    private Date orderTime;
    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm:ss")
    private Date orderBegin;
    /** 订单结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm:ss")
    private Date orderFinish;
    @Excel(name = "订单折扣")
    private BigDecimal orderDiscount;

    @Excel(name = "支付宝交易号")
    private String dealNumber;
    @Excel(name = "面试职位")
    private String post;
    private Integer jobStatus;
    private Integer commentStatus;
    @Excel(name = "订单图片")
    private String orderImg;

    public String getDealNumber() {
        return dealNumber;
    }

    public void setDealNumber(String dealNumber) {
        this.dealNumber = dealNumber;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrderState(String orderState)
    {
        this.orderState = orderState;
    }

    public String getOrderState()
    {
        return orderState;
    }

    public String getInterviewStatus() {
        return interviewStatus;
    }

    public void setInterviewStatus(String interviewStatus) {
        this.interviewStatus = interviewStatus;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getVersion()
    {
        return version;
    }
    public void setPlaceOrderDate(Date placeOrderDate)
    {
        this.placeOrderDate = placeOrderDate;
    }

    public Date getPlaceOrderDate()
    {
        return placeOrderDate;
    }
    public void setReservationDate(Date reservationDate)
    {
        this.reservationDate = reservationDate;
    }

    public Date getReservationDate()
    {
        return reservationDate;
    }
    public void setCancelDate(Date cancelDate)
    {
        this.cancelDate = cancelDate;
    }

    public Date getCancelDate()
    {
        return cancelDate;
    }
    public void setReplyDate(Date replyDate)
    {
        this.replyDate = replyDate;
    }

    public Date getReplyDate()
    {
        return replyDate;
    }
    public void setProductId(Integer productId)
    {
        this.productId = productId;
    }

    public Integer getProductId()
    {
        return productId;
    }
    public void setUserJobSeekerId(Integer userJobSeekerId)
    {
        this.userJobSeekerId = userJobSeekerId;
    }

    public Integer getUserJobSeekerId()
    {
        return userJobSeekerId;
    }
    public void setUserIntervieweeId(Integer userIntervieweeId)
    {
        this.userIntervieweeId = userIntervieweeId;
    }

    public Integer getUserIntervieweeId()
    {
        return userIntervieweeId;
    }
    public void setPayMode(String payMode)
    {
        this.payMode = payMode;
    }

    public String getPayMode()
    {
        return payMode;
    }
    public void setPreferentialMode(String preferentialMode)
    {
        this.preferentialMode = preferentialMode;
    }

    public String getPreferentialMode()
    {
        return preferentialMode;
    }
    public void setActualAmount(BigDecimal actualAmount)
    {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getActualAmount()
    {
        return actualAmount;
    }
    public void setOriginalCost(BigDecimal originalCost)
    {
        this.originalCost = originalCost;
    }

    public BigDecimal getOriginalCost()
    {
        return originalCost;
    }
    public void setOrderName(String orderName)
    {
        this.orderName = orderName;
    }

    public String getOrderName()
    {
        return orderName;
    }
    public void setOrderParticulars(String orderParticulars)
    {
        this.orderParticulars = orderParticulars;
    }

    public String getOrderParticulars()
    {
        return orderParticulars;
    }
    public void setOrderDuration(String orderDuration)
    {
        this.orderDuration = orderDuration;
    }

    public String getOrderDuration()
    {
        return orderDuration;
    }
    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getOrderType()
    {
        return orderType;
    }
    public void setOrderMType(String orderMType)
    {
        this.orderMType = orderMType;
    }

    public String getOrderMType()
    {
        return orderMType;
    }
    public void setOrderBegin(Date orderBegin)
    {
        this.orderBegin = orderBegin;
    }

    public Date getOrderBegin()
    {
        return orderBegin;
    }
    public void setOrderFinish(Date orderFinish)
    {
        this.orderFinish = orderFinish;
    }

    public Date getOrderFinish()
    {
        return orderFinish;
    }

    public BigDecimal getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(BigDecimal orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(Integer commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getOrderImg() {
        return orderImg;
    }

    public void setOrderImg(String orderImg) {
        this.orderImg = orderImg;
    }

    @Override
    public String toString() {
        return "KernelOrder{" +
                "id=" + id +
                ", orderNumber='" + orderNumber + '\'' +
                ", orderState='" + orderState + '\'' +
                ", interviewStatus='" + interviewStatus + '\'' +
                ", version='" + version + '\'' +
                ", placeOrderDate=" + placeOrderDate +
                ", reservationDate=" + reservationDate +
                ", cancelDate=" + cancelDate +
                ", replyDate=" + replyDate +
                ", productId=" + productId +
                ", userJobSeekerId=" + userJobSeekerId +
                ", userIntervieweeId=" + userIntervieweeId +
                ", payMode='" + payMode + '\'' +
                ", preferentialMode='" + preferentialMode + '\'' +
                ", actualAmount=" + actualAmount +
                ", originalCost=" + originalCost +
                ", orderName='" + orderName + '\'' +
                ", orderParticulars='" + orderParticulars + '\'' +
                ", orderDuration='" + orderDuration + '\'' +
                ", orderType='" + orderType + '\'' +
                ", orderMType='" + orderMType + '\'' +
                ", orderTime=" + orderTime +
                ", orderBegin=" + orderBegin +
                ", orderFinish=" + orderFinish +
                ", orderDiscount=" + orderDiscount +
                ", dealNumber='" + dealNumber + '\'' +
                ", post='" + post + '\'' +
                ", jobStatus=" + jobStatus +
                ", commentStatus=" + commentStatus +
                ", orderImg='" + orderImg + '\'' +
                '}';
    }
}
