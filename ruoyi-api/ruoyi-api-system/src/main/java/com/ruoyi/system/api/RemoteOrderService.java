package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.kernel.KernelOrderDTO;
import com.ruoyi.system.api.factory.RemoteOrderFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单服务
 *
 * @author lixin
 */
@FeignClient(contextId = "remoteOrderService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteOrderFallbackFactory.class)
public interface RemoteOrderService {
    /**
     * 保存系统日志
     *
     * @param kernelOrderDTO 订单实体
     * @param source 请求来源
     * @return 结果
     */
    @PutMapping("/kernel/order")
    public AjaxResult editOrder(@RequestBody KernelOrderDTO kernelOrderDTO, @RequestHeader(SecurityConstants.FROM_SOURCE)String source) throws Exception;



    @PostMapping("/kernel/order/select")
    public List<KernelOrderDTO> select( KernelOrderDTO kernelOrderDTO, @RequestHeader(SecurityConstants.FROM_SOURCE)String source) throws Exception;
}
