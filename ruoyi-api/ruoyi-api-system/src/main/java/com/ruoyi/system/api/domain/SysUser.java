package com.ruoyi.system.api.domain;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.annotation.Excel.ColumnType;
import com.ruoyi.common.core.annotation.Excel.Type;
import com.ruoyi.common.core.annotation.Excels;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.common.core.xss.Xss;

/**
 * 用户对象 sys_user
 *
 * @author ruoyi
 */
public class SysUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户序号", cellType = ColumnType.NUMERIC, prompt = "用户编号")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门编号", type = Type.IMPORT)
    private Long deptId;

    /** 用户账号 */
    @Excel(name = "登录名称")
    private String userName;

    /** 用户昵称 */
    @Excel(name = "用户名称")
    private String nickName;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别 */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    @Excel(name = "最后登录IP", type = Type.EXPORT)
    private String loginIp;

    /** 最后登录时间 */
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /** 部门对象 */
    @Excels({
        @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
        @Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT)
    })
    private SysDept dept;

    /** 角色对象 */
    private List<SysRole> roles;

    /** 角色组 */
    private Long[] roleIds;

    /** 岗位组 */
    private Long[] postIds;

    /** 角色ID */
    private Long roleId;

    /** 学历 */
    @Excel(name = "学历")
    private String qualification;

    /** 职位 */
    @Excel(name = "职位")
    private String position;

    /** 所属公司 */
    @Excel(name = "所属公司")
    private String company;

    /** 级别 */
    @Excel(name = "级别")
    private Long level;

    /** 工作经验等自我介绍 */
    @Excel(name = "工作经验等自我介绍")
    private String intro;

    /** 背景图 */
    @Excel(name = "背景图")
    private String backgroundImage;

    /** 教育经历介绍 */
    @Excel(name = "教育经历介绍")
    private String educationalExperience;

    /** 工作经历介绍 */
    @Excel(name = "工作经历介绍")
    private String workExperience;

    /** 海外经历介绍 */
    @Excel(name = "海外经历介绍")
    private String overseasExperience;

    /** 学术经历介绍 */
    @Excel(name = "学术经历介绍")
    private String academicExperience;

    /** 整体介绍 */
    @Excel(name = "整体介绍")
    private String overallIntroduction;

    /** 技能介绍 */
    @Excel(name = "技能介绍")
    private String skillIntroduction;

    /** 资质证书介绍 */
    @Excel(name = "资质证书介绍")
    private String certifications;

    /** 当前工作单位 */
    @Excel(name = "当前工作单位")
    private String currentWorkUnit;

    /** 擅长面试领域 */
    @Excel(name = "擅长面试领域")
    private String interviewField;

    /** 个人标签库 */
    @Excel(name = "个人标签库")
    private String personalTagLibrary;

    /** 本科毕业院校 */
    @Excel(name = "本科毕业院校")
    private String undergraduateInstitutions;

    /** 硕士毕业院校 */
    @Excel(name = "硕士毕业院校")
    private String graduateSchool;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String identificationNumber;

    /** 当前工作邮箱 */
    @Excel(name = "当前工作邮箱")
    private String currentWorkEmail;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String actualName;

    /** 学历认证状态 */
    @Excel(name = "学历认证状态")
    private Long degreeVerificationStatus;

    /** 身份证认证状态 */
    @Excel(name = "身份证认证状态")
    private Long identityCardAuthenticationStatus;

    /** 是否完成工作邮箱认证 */
    @Excel(name = "是否完成工作邮箱认证")
    private Long workEmailVerification;

    /** 资质证书认证 */
    @Excel(name = "资质证书认证")
    private Long qualificationCertificateCertification;

    /** chatbot次数 */
    @Excel(name = "chatbot次数")
    private Long memberChatbot;

    /** 真人聊天次数 */
    @Excel(name = "真人聊天次数")
    private Long memberZhenren;

    /** ai模拟次数 */
    @Excel(name = "ai模拟次数")
    private Long memberAiSimulation;

    /** ai实时次数 */
    @Excel(name = "ai实时次数")
    private Long memberAiRealTime;

    /** 身份状态（0下线，1上线） */
    @Excel(name = "身份状态", readConverterExp = "0=下线，1上线")
    private String identityStatus;

    public SysUser()
    {

    }

    public SysUser(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public boolean isAdmin()
    {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }

    public Long getDeptId()
    {
        return deptId;
    }

    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    @Xss(message = "用户昵称不能包含脚本字符")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    public String getPhonenumber()
    {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber)
    {
        this.phonenumber = phonenumber;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getAvatar()
    {
        return avatar;
    }

    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getLoginIp()
    {
        return loginIp;
    }

    public void setLoginIp(String loginIp)
    {
        this.loginIp = loginIp;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }

    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public SysDept getDept()
    {
        return dept;
    }

    public void setDept(SysDept dept)
    {
        this.dept = dept;
    }

    public List<SysRole> getRoles()
    {
        return roles;
    }

    public void setRoles(List<SysRole> roles)
    {
        this.roles = roles;
    }

    public Long[] getRoleIds()
    {
        return roleIds;
    }

    public void setRoleIds(Long[] roleIds)
    {
        this.roleIds = roleIds;
    }

    public Long[] getPostIds()
    {
        return postIds;
    }

    public void setPostIds(Long[] postIds)
    {
        this.postIds = postIds;
    }

    public Long getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }

    public String getQualification()
    {
        return qualification;
    }
    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getPosition()
    {
        return position;
    }
    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getCompany()
    {
        return company;
    }
    public void setLevel(Long level)
    {
        this.level = level;
    }

    public Long getLevel()
    {
        return level;
    }
    public void setIntro(String intro)
    {
        this.intro = intro;
    }

    public String getIntro()
    {
        return intro;
    }
    public void setBackgroundImage(String backgroundImage)
    {
        this.backgroundImage = backgroundImage;
    }

    public String getBackgroundImage()
    {
        return backgroundImage;
    }
    public void setEducationalExperience(String educationalExperience)
    {
        this.educationalExperience = educationalExperience;
    }

    public String getEducationalExperience()
    {
        return educationalExperience;
    }
    public void setWorkExperience(String workExperience)
    {
        this.workExperience = workExperience;
    }

    public String getWorkExperience()
    {
        return workExperience;
    }
    public void setOverseasExperience(String overseasExperience)
    {
        this.overseasExperience = overseasExperience;
    }

    public String getOverseasExperience()
    {
        return overseasExperience;
    }
    public void setAcademicExperience(String academicExperience)
    {
        this.academicExperience = academicExperience;
    }

    public String getAcademicExperience()
    {
        return academicExperience;
    }
    public void setOverallIntroduction(String overallIntroduction)
    {
        this.overallIntroduction = overallIntroduction;
    }

    public String getOverallIntroduction()
    {
        return overallIntroduction;
    }
    public void setSkillIntroduction(String skillIntroduction)
    {
        this.skillIntroduction = skillIntroduction;
    }

    public String getSkillIntroduction()
    {
        return skillIntroduction;
    }
    public void setCertifications(String certifications)
    {
        this.certifications = certifications;
    }

    public String getCertifications()
    {
        return certifications;
    }
    public void setCurrentWorkUnit(String currentWorkUnit)
    {
        this.currentWorkUnit = currentWorkUnit;
    }

    public String getCurrentWorkUnit()
    {
        return currentWorkUnit;
    }
    public void setInterviewField(String interviewField)
    {
        this.interviewField = interviewField;
    }

    public String getInterviewField()
    {
        return interviewField;
    }
    public void setPersonalTagLibrary(String personalTagLibrary)
    {
        this.personalTagLibrary = personalTagLibrary;
    }

    public String getPersonalTagLibrary()
    {
        return personalTagLibrary;
    }
    public void setUndergraduateInstitutions(String undergraduateInstitutions)
    {
        this.undergraduateInstitutions = undergraduateInstitutions;
    }

    public String getUndergraduateInstitutions()
    {
        return undergraduateInstitutions;
    }
    public void setGraduateSchool(String graduateSchool)
    {
        this.graduateSchool = graduateSchool;
    }

    public String getGraduateSchool()
    {
        return graduateSchool;
    }
    public void setIdentificationNumber(String identificationNumber)
    {
        this.identificationNumber = identificationNumber;
    }

    public String getIdentificationNumber()
    {
        return identificationNumber;
    }
    public void setCurrentWorkEmail(String currentWorkEmail)
    {
        this.currentWorkEmail = currentWorkEmail;
    }

    public String getCurrentWorkEmail()
    {
        return currentWorkEmail;
    }
    public void setActualName(String actualName)
    {
        this.actualName = actualName;
    }

    public String getActualName()
    {
        return actualName;
    }
    public void setDegreeVerificationStatus(Long degreeVerificationStatus)
    {
        this.degreeVerificationStatus = degreeVerificationStatus;
    }

    public Long getDegreeVerificationStatus()
    {
        return degreeVerificationStatus;
    }
    public void setIdentityCardAuthenticationStatus(Long identityCardAuthenticationStatus)
    {
        this.identityCardAuthenticationStatus = identityCardAuthenticationStatus;
    }

    public Long getIdentityCardAuthenticationStatus()
    {
        return identityCardAuthenticationStatus;
    }
    public void setWorkEmailVerification(Long workEmailVerification)
    {
        this.workEmailVerification = workEmailVerification;
    }

    public Long getWorkEmailVerification()
    {
        return workEmailVerification;
    }
    public void setQualificationCertificateCertification(Long qualificationCertificateCertification)
    {
        this.qualificationCertificateCertification = qualificationCertificateCertification;
    }

    public Long getQualificationCertificateCertification()
    {
        return qualificationCertificateCertification;
    }
    public void setMemberChatbot(Long memberChatbot)
    {
        this.memberChatbot = memberChatbot;
    }

    public Long getMemberChatbot()
    {
        return memberChatbot;
    }
    public void setMemberZhenren(Long memberZhenren)
    {
        this.memberZhenren = memberZhenren;
    }

    public Long getMemberZhenren()
    {
        return memberZhenren;
    }
    public void setMemberAiSimulation(Long memberAiSimulation)
    {
        this.memberAiSimulation = memberAiSimulation;
    }

    public Long getMemberAiSimulation()
    {
        return memberAiSimulation;
    }
    public void setMemberAiRealTime(Long memberAiRealTime)
    {
        this.memberAiRealTime = memberAiRealTime;
    }

    public Long getMemberAiRealTime()
    {
        return memberAiRealTime;
    }
    public void setIdentityStatus(String identityStatus)
    {
        this.identityStatus = identityStatus;
    }

    public String getIdentityStatus()
    {
        return identityStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("deptId", getDeptId())
            .append("userName", getUserName())
            .append("nickName", getNickName())
            .append("email", getEmail())
            .append("phonenumber", getPhonenumber())
            .append("sex", getSex())
            .append("avatar", getAvatar())
            .append("password", getPassword())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("loginIp", getLoginIp())
            .append("loginDate", getLoginDate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("dept", getDept())
            .append("qualification", getQualification())
            .append("position", getPosition())
            .append("company", getCompany())
            .append("level", getLevel())
            .append("intro", getIntro())
            .append("remark", getRemark())
            .append("backgroundImage", getBackgroundImage())
            .append("educationalExperience", getEducationalExperience())
            .append("workExperience", getWorkExperience())
            .append("overseasExperience", getOverseasExperience())
            .append("academicExperience", getAcademicExperience())
            .append("overallIntroduction", getOverallIntroduction())
            .append("skillIntroduction", getSkillIntroduction())
            .append("certifications", getCertifications())
            .append("currentWorkUnit", getCurrentWorkUnit())
            .append("interviewField", getInterviewField())
            .append("personalTagLibrary", getPersonalTagLibrary())
            .append("undergraduateInstitutions", getUndergraduateInstitutions())
            .append("graduateSchool", getGraduateSchool())
            .append("identificationNumber", getIdentificationNumber())
            .append("currentWorkEmail", getCurrentWorkEmail())
            .append("actualName", getActualName())
            .append("degreeVerificationStatus", getDegreeVerificationStatus())
            .append("identityCardAuthenticationStatus", getIdentityCardAuthenticationStatus())
            .append("workEmailVerification", getWorkEmailVerification())
            .append("qualificationCertificateCertification", getQualificationCertificateCertification())
            .append("memberChatbot", getMemberChatbot())
            .append("memberZhenren", getMemberZhenren())
            .append("memberAiSimulation", getMemberAiSimulation())
            .append("memberAiRealTime", getMemberAiRealTime())
            .append("identityStatus", getIdentityStatus())
            .toString();
    }
}

