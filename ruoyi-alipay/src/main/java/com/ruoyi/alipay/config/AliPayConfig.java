package com.ruoyi.alipay.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "spring.aliyun.pay")
public class AliPayConfig {
    private String appId;
    private String privateKey;
    private String alipayPublicKey;
    private String notifyUrl;


}