package com.ruoyi.alipay.controller;

import cn.hutool.json.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.ruoyi.alipay.config.AliPayConfig;
import com.ruoyi.alipay.domain.AliPayOrder;
//import com.ruoyi.common.core.constant.SecurityConstants;
//import com.ruoyi.common.core.web.domain.AjaxResult;
//import com.ruoyi.common.core.web.page.TableDataInfo;
//import com.ruoyi.system.api.RemoteOrderService;
//import com.ruoyi.system.api.domain.kernel.KernelOrderDTO;
//import com.ruoyi.system.api.RemoteOrderService;
//import com.ruoyi.system.api.RemoteOrderService;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.RemoteOrderService;
import com.ruoyi.system.api.domain.kernel.KernelOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//支付宝支付接口
@Component
@Controller
@RequestMapping("/alipay")
public class AlipayController {

    @Value("${spring.aliyun.pay.serverUrl}")
    private String serverUrl;
    @Value("${spring.aliyun.pay.appId}")
    private String appId;
    @Value("${spring.aliyun.pay.privateKey}")
    private String privateKey;
    @Value("${spring.aliyun.pay.alipayPublicKey}")
    private String alipayPublicKey;
   @Autowired
   private RemoteOrderService remoteOrderService;


    //创建订单接口
    @ResponseBody
    @GetMapping("/create")
    public String create(){

        //创建连接
        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl,appId,privateKey,"json", "UTF-8",alipayPublicKey,"RSA2");
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();

        //订单信息
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", "202100012260165");
        bizContent.put("total_amount", 5.00);
        bizContent.put("subject", "测试");
        bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");

        request.setBizContent(bizContent.toString());

        AlipayTradePagePayResponse response = null;
        try {
            response = alipayClient.pageExecute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        if(response.isSuccess()){
            //更改订单状态
           //int i=iKernelOrderService.updateKernelOrder();
            System.out.println("成功");
            //成功
            return  response.getBody();
        } else {
            //失败
            return  "调用失败";
        }
    }










    private static final String GATEWAY_URL = "https://openapi-sandbox.dl.alipaydev.com/gateway.do";
    private static final String FORMAT = "JSON";
    private static final String CHARSET = "UTF-8";
    //签名方式
    private static final String SIGN_TYPE = "RSA2";

    @Resource
    private AliPayConfig aliPayConfig;



    @GetMapping("/pay") // &subject=xxx&traceNo=xxx&totalAmount=xxx
    public String pay(AliPayOrder aliPayOrder, HttpServletResponse httpResponse) throws Exception {
        System.out.println("进入");
        // 1. 创建Client，通用SDK提供的Client，负责调用支付宝的API
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL, aliPayConfig.getAppId(),
                aliPayConfig.getPrivateKey(), FORMAT, CHARSET, aliPayConfig.getAlipayPublicKey(), SIGN_TYPE);

        // 2. 创建 Request并设置Request参数
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();  // 发送请求的 Request类
        request.setNotifyUrl(aliPayConfig.getNotifyUrl());
        JSONObject bizContent = new JSONObject();
        bizContent.set("out_trade_no", aliPayOrder.getOrderNumber());  // 我们自己生成的订单编号
        bizContent.set("total_amount", aliPayOrder.getActualAmount()); // 订单的总金额
        bizContent.set("subject", aliPayOrder.getOrderName());   // 支付的名称
        bizContent.set("product_code", "FAST_INSTANT_TRADE_PAY");  // 固定配置
        request.setBizContent(bizContent.toString());

        // 执行请求，拿到响应的结果，返回给浏览器
        String form = "";
        try {
            form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
            System.out.println(form+"输出");
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + CHARSET);
        httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
        return form;
    }

    @PostMapping("/notify")  // 注意这里必须是POST接口
    public String payNotify(HttpServletRequest request) throws Exception {
        if (request.getParameter("trade_status").equals("TRADE_SUCCESS")) {
            System.out.println("=========支付宝异步回调========");

            Map<String, String> params = new HashMap<>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (String name : requestParams.keySet()) {
                params.put(name, request.getParameter(name));
                // System.out.println(name + " = " + request.getParameter(name));
            }

            String outTradeNo = params.get("out_trade_no");
            String gmtPayment = params.get("gmt_payment");
            String alipayTradeNo = params.get("trade_no");

            String sign = params.get("sign");
            String content = AlipaySignature.getSignCheckContentV1(params);
            boolean checkSignature = AlipaySignature.rsa256CheckContent(content, sign, aliPayConfig.getAlipayPublicKey(), "UTF-8"); // 验证签名
            // 支付宝验签
            if (checkSignature) {
                // 验签通过
                System.out.println("交易名称: " + params.get("subject"));
                System.out.println("交易状态: " + params.get("trade_status"));
                System.out.println("支付宝交易凭证号: " + params.get("trade_no"));
                System.out.println("商户订单号: " + params.get("out_trade_no"));
                System.out.println("交易金额: " + params.get("total_amount"));
                System.out.println("买家在支付宝唯一id: " + params.get("buyer_id"));
                System.out.println("买家付款时间: " + params.get("gmt_payment"));
                System.out.println("买家付款金额: " + params.get("buyer_pay_amount"));
                System.out.println("回调成功");
                // 查询订单
                KernelOrderDTO kernelOrder =new KernelOrderDTO();
                kernelOrder.setOrderNumber(outTradeNo);
                List<KernelOrderDTO> list=remoteOrderService.select(kernelOrder, SecurityConstants.INNER);


                if (list != null && !list.isEmpty()) {
                    KernelOrderDTO kernelOrder1 =new KernelOrderDTO();
                    //支付时间
                    kernelOrder1.setOrderBegin(new Date());
                    kernelOrder1.setDealNumber(alipayTradeNo);
                    kernelOrder1.setOrderState("0");
                    for (int i = 0; i < list.size(); i++) {
                        kernelOrder1.setId(list.get(i).getId());

                    }

                    AjaxResult ajaxResult = remoteOrderService.editOrder(kernelOrder1, SecurityConstants.INNER);
                System.out.println(
                        ajaxResult+"测试"
                );
                    System.out.println("走完");
                }
            }
        }
        return "success";
    }
}
