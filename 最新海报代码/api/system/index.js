import upload from '@/utils/upload'
import request from '@/utils/request'

export function deviceNumber(userId){
	return request({
		url:`/system/order/myYunJi?userId=${userId}`,
		method:'get'
	})
}

export function parationNumber(userId){
	return request({
		url:`/system/home/offLineYunji?userId=${userId}`,
		method:'get'
	})
}


export function onlineNumber(userId){
	return request({
		url:`/system/home/onLineYunji?userId=${userId}`,
		method:'get'
	})
}

export function achievementNum(userId){
	return request({
		url:`/system/agency/teamPerformance?fid=${userId}`,
		method:'get'
	})
}

export function peopleNum(userId){
	return request({
		url:`/system/agency/teamNumber?fid=${userId}`,
		method:'get'
	})
}

export function teamChannelsNum(userId){
	return request({
		url:`/system/agency/teamChannel?userId=${userId}`,
		method:'get'
	})
}