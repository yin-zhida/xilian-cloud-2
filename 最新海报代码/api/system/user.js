import upload from '@/utils/upload'
import request from '@/utils/request'

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/system/user/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/system/user/profile',
    method: 'get',
  })
}

export function deBank (data) {
  return request({
    url: '/system/card/deBank/'+data,
    method: 'delete',
  })
}



export function addBank(data) {
  return request({
    url: '/system/card/addBank',
    method: 'POST',
	data
  })
}
export function putBank(data) {
  return request({
    url: '/system/card/putBank',
    method: 'put',
	data
  })
}



// 查询用户收款信息
export function getColletion(data) {
  return request({
    url: '/system/card/myBank',
    method: 'get',
	params: data
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/system/user/profile',
    method: 'put',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return upload({
    url: '/system/user/profile/avatar',
    name: data.name,
    filePath: data.filePath
  })
}
// *********************订单页面***************************
//订单添加提交
export function addOrder(orderName,orderNumber,orderPrice,orderState,orderTime,userId,proof,account) {
	// 以json方式传递
	 const data = {
	    orderName,
	    orderNumber,
	    orderPrice,
	    orderState,
	    orderTime,
	    userId,
		proof,
		account
	  };
  return request({
    url: `/system/order/addOrder`,
    method: 'post',
	headers: {
	    'Content-Type': 'application/json', // 根据后端接口要求设置合适的 Content-Type
	},
	data: JSON.stringify(data)
  });
}
// 交易凭证上传
export function uploadPictrue (data){
	const formData = new FormData();
	formData.append('file', data.file); // 将文件添加到FormData对象中
	formData.append('name', data.name); // 添加其他参数到FormData对象中
	return request({
		url:'/common/upload',
		 method: 'post',
		 data: formData,
		 headers: {
			'Content-Type': 'multipart/form-data' // 设置请求头为multipart/form-data类型
		}
	})
}
// 查看商品
export function robotList(){
	return request({
		url:'/system/robot/robotList',
		method:'get',
	});
}
//*************查看邀请人页面*********************
// 查询邀请人
export function myInvite(fid,grade){
	return request({
		url: `/system/relation/myInvite?fid=${fid}&grade=${grade}`,
		method:'get',
	})
}
//***************订单列表页面***********************
// 订单查询（所有购买的列表展现）
export function myOrderList(params){
	return request({
		url:`/system/order/myOrderList?${params}`,
		method:'get'
	});
}
// ****************我的收益页面***********************
// 查看我的收益
export function Benegits(userId){
	return request({
		url:`/system/bao/myQian?userId=${userId}`,
		method:'get'
	})
}
//查看我的提现记录
export function withdrawalRecords(userId){
	return request({
		url:`/system/withdraw/myWithdraw?userId=${userId}`,
		method:'get'
	})
} 
// 支付类型
export function myBank(userId,accountType){
	return request({
		url:`/system/card/mybank?userId=${userId}&accountType=${accountType}`,
		method:'get'
	})
}
// 提现
export function addWithdraw(amount,cardId,userId){
	const data = {
		amount,
		cardId,
		userId
	}
	return request({
		url:'/system/withdraw/addWithdraw',
		method:'post',
		headers: {
		    'Content-Type': 'application/json', // 根据后端接口要求设置合适的 Content-Type
		},
		data: JSON.stringify(data)
	})
}

// 获取信息
export function userID(){
	return request({
		url:'/houqu',
		method:'get'
	})
}
//收款信息编辑修改
export function getInfos(id){
	console.log('lllllllll',id);
	return request({
		url:`/system/card/getInfos/${id}`,
		method:'get'
	})
}
