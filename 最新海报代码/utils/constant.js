const constant = {
  avatar: "vuex_avatar",
  name: "vuex_name",
  user: "vuex_user",
  roles: "vuex_roles",
  permissions: "vuex_permissions",
};

export default constant;
